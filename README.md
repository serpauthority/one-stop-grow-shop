# How to Set Up Your First Indoor Garden Using Hydroponics

Hydroponics is not a new way to grow plants, in fact it has been around for decades. Having said this, it is certainly one of the most high tech ways to grow an optimal crop, and is preferred by many indoor gardeners looking to maximise their yields and increase their profitability. There are many benefits to this indoor growing method, and if you are considering giving it a try in your own home then this guide will help you on your way to perfect yields and a profitable harvest.

## What is hydroponics?

Most crops are grown in the ground using soil, fertiliser and some kind of irrigation system. Hydroponics works very differently, and most hydroponically grown crops will never have their roots touch soil. Rather than growing plants so they are rooted into soil, hydroponically grown crops are rooted into an inert substrate such as perlite or ceramic pebbles, and their roots are either completely immersed in water and a nutrient solution, or water and nutrients are delivered in small but constant doses to the root system.

This is quite a technical process, but it is one that is fairly easy to master with a little practice. Most amateur or first time indoor gardeners will notice great results from their very first crop, and from there will want to expand on their current system to produce even bigger harvests. There are several different kinds of hydroponics systems, but most beginners prefer to keep to something cost effective and relatively simple, such as deep water culture systems (DWC).

## Setting up a deep water culture system at home

DWCs are known to be one of the easiest and most manageable options for first time and beginner growers. They are simple and cheap to maintain, and can be set up and running for as little as £150 or less. Their low cost and simplicity is why we would recommend them to most beginners looking to gain experience in hydroponics.

A DWC consists of a large reservoir which needs to be big enough to accommodate the full size of the root balls growing inside. The size you need will depend on the species and cultivar you are growing, so always do your research to make sure you choose the right size. The plant’s roots are suspended in the water and rooted into an inert substrate which does not react with the water and does not contaminate it in any way. As water reservoirs don’t offer the right levels of oxygen that plants need to prevent their roots from rotting, you will need to also include airstones to aerate the water properly.

When you are growing plants indoors, one of the most important things you need to replicate is their natural light conditions. All plants need light to grow, and the amount, duration and intensity of light they need will depend on the species you are growing. <span style="text-decoration: underline;">_**[LED grow lights](https://www.onestopgrowshop.co.uk/grow-lights/grow-lights-led-grow-lights.html)**_</span> are standard pieces of kit to include in your hydroponics system, and you might find that they are one of the most expensive components you will need to buy. Grow lights allow you to control the growing conditions so your plants can have everything they need to thrive.

In addition to water and light, you will also need to supplement your plants with liquid feeds to provide them with all the nutrients and minerals they need. Water doesn't provide any nutrients to plants, so you will need to mix a special hydroponics fertiliser into your water in the correct concentrations for each growth stage of your plants’ development.

To keep things really simple for beginners, you can try buying a hydroponics kit which comes with everything you need to get started. These kits offer genuine value for money for those looking for an entry level set up, but they also give you loads of bang for your buck and you will quickly be able to start harvesting a massive, healthy crop from your plants. These kits usually come with a grow tent, [the right lights](https://salsa.debian.org/serpauthority/one-stop-grow-shop) for the size of the grow tent, a water reservoir, air brick, and bottles of fertiliser and are often at discounted prices when compared to buying everything you need separately.

### Resources:

*   [LED Grow Lights UK - Gitee](https://gitee.com/serpauthority/one-stop-grow-shop)
*   [Grow Lamp - GNOME](https://gitlab.gnome.org/serpauthority/one-stop-grow-shop)
*   [Grow Lights for Sale - Montera34](https://code.montera34.com/serpauthority/one-stop-grow-shop)
*   [Sunmaster Grow Lights - Renku](https://renkulab.io/gitlab/serp-authority/one-stop-grow-shop)
